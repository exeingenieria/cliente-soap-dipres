package cl.exe.interoperabilidad.dipres.util;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import cl.exe.interoperabilidad.dipres.exception.ConexionException;


public final class ConexionUtil {

	public static boolean validarConexion(String urlStr) throws ConexionException, MalformedURLException{
		
		URL url = null;
		try {
			url = new URL(urlStr);
		} catch (MalformedURLException e1) {
			throw new MalformedURLException("Url '" + urlStr + "' malformada");
		}		
		
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			con.setConnectTimeout(2000);

			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (java.net.SocketTimeoutException e) {
			throw new ConexionException("No se ha podido establecer conexión con " + urlStr);
		} catch (java.io.IOException e) {
			throw new ConexionException("No se ha podido establecer conexión con " + urlStr);
		}
	}
	
}
