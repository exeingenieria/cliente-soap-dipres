
package cl.exe.interoperabilidad.dipres.siapiop;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.exe.interoperabilidad.dipres.siapiop package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _HaciendaNotificaADipres_QNAME = new QName("http://caroli.reddipres.cl/pkg_hacienda.Notificacion_Hacienda_a_Dipres", "HaciendaNotificaADipres");
    private final static QName _HaciendaNotificaADipresResponse_QNAME = new QName("http://caroli.reddipres.cl/pkg_hacienda.Notificacion_Hacienda_a_Dipres", "HaciendaNotificaADipresResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.exe.interoperabilidad.dipres.siapiop
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Antecedente }
     * 
     */
    public Antecedente createAntecedente() {
        return new Antecedente();
    }

    /**
     * Create an instance of {@link Decreto }
     * 
     */
    public Decreto createDecreto() {
        return new Decreto();
    }

    /**
     * Create an instance of {@link HaciendaNotificaADipres }
     * 
     */
    public HaciendaNotificaADipres createHaciendaNotificaADipres() {
        return new HaciendaNotificaADipres();
    }

    /**
     * Create an instance of {@link HaciendaNotificaADipresResponse }
     * 
     */
    public HaciendaNotificaADipresResponse createHaciendaNotificaADipresResponse() {
        return new HaciendaNotificaADipresResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HaciendaNotificaADipres }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://caroli.reddipres.cl/pkg_hacienda.Notificacion_Hacienda_a_Dipres", name = "HaciendaNotificaADipres")
    public JAXBElement<HaciendaNotificaADipres> createHaciendaNotificaADipres(HaciendaNotificaADipres value) {
        return new JAXBElement<HaciendaNotificaADipres>(_HaciendaNotificaADipres_QNAME, HaciendaNotificaADipres.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HaciendaNotificaADipresResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://caroli.reddipres.cl/pkg_hacienda.Notificacion_Hacienda_a_Dipres", name = "HaciendaNotificaADipresResponse")
    public JAXBElement<HaciendaNotificaADipresResponse> createHaciendaNotificaADipresResponse(HaciendaNotificaADipresResponse value) {
        return new JAXBElement<HaciendaNotificaADipresResponse>(_HaciendaNotificaADipresResponse_QNAME, HaciendaNotificaADipresResponse.class, null, value);
    }

}
