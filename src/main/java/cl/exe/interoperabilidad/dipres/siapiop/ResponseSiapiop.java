package cl.exe.interoperabilidad.dipres.siapiop;

public class ResponseSiapiop {

	private String codigo;
	private String msg;

	public ResponseSiapiop() {
		super();
	}

	public ResponseSiapiop(String codigo, String msg) {
		super();
		this.codigo = codigo;
		this.msg = msg;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
