package cl.exe.interoperabilidad.dipres.siapiop;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import cl.exe.interoperabilidad.dipres.exception.ConexionException;
import cl.exe.interoperabilidad.dipres.siapiop.handlers.HeaderHandler;
import cl.exe.interoperabilidad.dipres.util.ConexionUtil;

public class Notificador implements Serializable {

	private static final long serialVersionUID = -3843643575139821023L;
	public static final String EXE_PROXY_SOAP_NOTIFICACION_DIPRES_ERROR_GENERICO = "2003";
	public static final String EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE = "2001";


	public ResponseSiapiop notificar(HaciendaNotificaADipres request) {
		try {
			PropertiesConfiguration config = new PropertiesConfiguration("soap-dipres.properties");
			String username = (String) config.getProperty("username");
			String password = (String) config.getProperty("password");
			String nonce = (String) config.getProperty("nonce");
			String created = (String) config.getProperty("created");
			String url = (String) config.getProperty("url");
			String namespace = (String) config.getProperty("namespace");
			String localpart = (String) config.getProperty("localpart");
			
			if (ConexionUtil.validarConexion(url) ) {
			
				URL wsdl = new URL(url);
				QName qname = new QName(namespace, localpart);
				
				Service service = Service.create(wsdl, qname);
				HaciendaNotificaADipresWSDPortType port = service.getPort(HaciendaNotificaADipresWSDPortType.class);
	
				List<Handler> handlerChain = new ArrayList<Handler>();
				HeaderHandler headerHandler = new HeaderHandler();
	
				headerHandler.setWsseUsername(username);
				headerHandler.setWssePassword(password);
				headerHandler.setWsseNonce(nonce);
				headerHandler.setWsseCreated(created);
				handlerChain.add(headerHandler);
				
				Binding binding = ((BindingProvider) port).getBinding();
				binding.setHandlerChain(handlerChain);
	
				Holder<String> codigo = new Holder<String>();
				Holder<String> msg = new Holder<String>();
	
				port.haciendaNotificaADipres(request.getExpediente(), request.getEstadoTramitacion(), request.getDecreto(),
						request.getAntecedente(), request.getMotivo(), codigo, msg);
	
				return new ResponseSiapiop(codigo.value, msg.value);
			} else {
				return new ResponseSiapiop(EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE, "El servicio de Notificación Dipres no está disponible");
			}
		} catch (ConfigurationException e) {
			e.printStackTrace();
			return new ResponseSiapiop(EXE_PROXY_SOAP_NOTIFICACION_DIPRES_ERROR_GENERICO, "Problemas en las configuraciones del servicio");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return new ResponseSiapiop(EXE_PROXY_SOAP_NOTIFICACION_DIPRES_ERROR_GENERICO, "La url del servicio no es válida");
		}catch (ConexionException e) {
			e.printStackTrace();
			return new ResponseSiapiop(EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			String mensaje = "El servicio de Notificación Dipres no está disponible";
			
			if ( e.getMessage() != null && e.getMessage().length() > 0) {
				if ( e.getMessage().length() > 400) {
					return new ResponseSiapiop(EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE, e.getMessage().substring(0, 397) + "...");
				} else {
					return new ResponseSiapiop(EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE, e.getMessage() );
				}
			}
			return new ResponseSiapiop(EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE, mensaje);
		}

	}
}
