package cl.exe.interoperabilidad.dipres.siapiop.handlers;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.MessageContext.Scope;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.ws.soap.SOAPFaultException;

/**
 *
 * @author www.javadb.com
 */
public class HeaderHandler implements SOAPHandler<SOAPMessageContext> {

	private String wsseUsername;
	private String wssePassword;
	private String wsseNonce;
	private String wsseCreated;

	private static final String WSSE_NS_URI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final QName QNAME_WSSE_USERNAMETOKEN = new QName(WSSE_NS_URI, "UsernameToken");
	private static final QName QNAME_WSSE_USERNAME = new QName(WSSE_NS_URI,	"Username");
	private static final QName QNAME_WSSE_PASSWORD = new QName(WSSE_NS_URI,	"Password");

	public boolean handleMessage(SOAPMessageContext ctx) {

		Boolean outboundProperty = (Boolean) ctx
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (outboundProperty.booleanValue()) {

			SOAPMessage message = ctx.getMessage();

			try {

				SOAPPart soapart = ctx.getMessage().getSOAPPart();
				SOAPEnvelope envelope = soapart.getEnvelope();
				SOAPBody body = ctx.getMessage().getSOAPBody();
				
				envelope.addNamespaceDeclaration("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
				envelope.addNamespaceDeclaration("pkg", "http://caroli.reddipres.cl/pkg_hacienda.Notificacion_Hacienda_a_Dipres");
				
				envelope.removeNamespaceDeclaration("S");
				
				envelope.setPrefix("soapenv");
				body.setPrefix("soapenv");
				
				modifyBody(body.getChildElements());
				
				if (envelope.getHeader() != null) {
					envelope.getHeader().detachNode();
				}
				SOAPHeader header = envelope.addHeader();

				SOAPElement security = header.addChildElement("Security", "wsse", WSSE_NS_URI);
				security.addAttribute(new QName("soapenv:mustUnderstand"), "1");
				security.addAttribute(new QName("xmlns:wsse"), WSSE_NS_URI);
				security.addNamespaceDeclaration("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
				SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
				usernameToken.addAttribute(new QName("wsu:Id"), "UsernameToken-249DF8B9F9E84B6B0115403866480271");

				SOAPElement username = usernameToken.addChildElement("Username", "wsse");
				username.addTextNode(this.getWsseUsername());

				SOAPElement password = usernameToken.addChildElement("Password", "wsse");
				password.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
				password.addTextNode(this.getWssePassword());
				
				SOAPElement nonce = usernameToken.addChildElement("Nonce", "wsse");
				nonce.setAttribute("EncodingType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
				nonce.addTextNode(this.getWsseNonce());
				
				SOAPElement created = usernameToken.addChildElement("Created", "wsu");
				created.addTextNode(this.getWsseCreated());
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {

			try {
				SOAPHeader header = ctx.getMessage().getSOAPHeader();
				Iterator<?> headerElements = header.examineAllHeaderElements();
				while (headerElements.hasNext()) {
					SOAPHeaderElement headerElement = (SOAPHeaderElement) headerElements
							.next();
					if (headerElement.getElementName().getLocalName()
							.equals("Security")) {
						SOAPHeaderElement securityElement = headerElement;
						Iterator<?> it2 = securityElement.getChildElements();
						while (it2.hasNext()) {
							Node soapNode = (Node) it2.next();
							if (soapNode instanceof SOAPElement) {
								SOAPElement element = (SOAPElement) soapNode;
								QName elementQname = element.getElementQName();
								if (QNAME_WSSE_USERNAMETOKEN
										.equals(elementQname)) {
									SOAPElement usernameTokenElement = element;
									wsseUsername = getFirstChildElementValue(
											usernameTokenElement,
											QNAME_WSSE_USERNAME);
									wssePassword = getFirstChildElementValue(
											usernameTokenElement,
											QNAME_WSSE_PASSWORD);
									break;
								}
							}

							if (wsseUsername != null) {
								break;
							}
						}
					}
					
					ctx.put("USERNAME", wsseUsername);
					ctx.setScope("USERNAME", Scope.APPLICATION);

					ctx.put("PASSWORD", wssePassword);
					ctx.setScope("PASSWORD", Scope.APPLICATION);					
				}
				
				
			} catch (Exception e) {
				System.out.println("Error reading SOAP message context: " + e);
				e.printStackTrace();
			}
		}

		return outboundProperty;

	}

	public String getWsseUsername() {
		return wsseUsername;
	}

	public void setWsseUsername(String wsseUsername) {
		this.wsseUsername = wsseUsername;
	}

	public void setWssePassword(String wssePassword) {
		this.wssePassword = wssePassword;
	}
	public String getWssePassword() {
		return wssePassword;
	}

	public String getWsseNonce() {
		return wsseNonce;
	}

	public void setWsseNonce(String wsseNonce) {
		this.wsseNonce = wsseNonce;
	}

	public String getWsseCreated() {
		return wsseCreated;
	}

	public void setWsseCreated(String wsseCreated) {
		this.wsseCreated = wsseCreated;
	}

	public Set<QName> getHeaders() {
		// The code below is added on order to invoke Spring secured WS.
		// Otherwise,
		// http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd
		// won't be recognised
		final QName securityHeader = new QName(
				WSSE_NS_URI,
				"Security", "wsse");

		final HashSet<QName> headers = new HashSet<QName>();
		headers.add(securityHeader);

		return headers;
	}

	public boolean handleFault(SOAPMessageContext context) {
		// throw new UnsupportedOperationException("Not supported yet.");
		return true;
	}

	public void close(MessageContext context) {
		// throw new UnsupportedOperationException("Not supported yet.");
	}

	private void generateSOAPErrMessage(SOAPMessage msg, String reason) {
		try {
			SOAPBody soapBody = msg.getSOAPPart().getEnvelope().getBody();
			SOAPFault soapFault = soapBody.addFault();
			soapFault.setFaultString(reason);
			throw new SOAPFaultException(soapFault);
		} catch (SOAPException e) {
		}
	}

	private String getFirstChildElementValue(SOAPElement soapElement,
			QName qNameToFind) {
		String value = null;
		Iterator<SOAPElement> it = soapElement.getChildElements(qNameToFind);
		while (it.hasNext()) {
			SOAPElement element = it.next(); // use first
			value = element.getValue();
		}
		return value;
	}
	
	private void modifyBody(Iterator childElements) {
		while (childElements.hasNext()) {
			final Object childElementNode = childElements.next();
			if (childElementNode instanceof SOAPElement) {
				SOAPElement soapElement = (SOAPElement) childElementNode;

				// set desired namespace body element prefix
				if ("HaciendaNotificaADipres".equals(soapElement.getLocalName())) {
					soapElement.setPrefix("pkg");
					soapElement.removeNamespaceDeclaration("ns2");
				}
				
				soapElement.removeNamespaceDeclaration("xsi");
				soapElement.removeAttribute("xsi:nil");

				// recursively set desired namespace prefix entries in child
				// elements
				modifyBody(soapElement.getChildElements());
			}
		}
	}
}