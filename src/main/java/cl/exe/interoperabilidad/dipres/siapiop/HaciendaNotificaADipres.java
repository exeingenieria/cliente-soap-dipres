
package cl.exe.interoperabilidad.dipres.siapiop;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HaciendaNotificaADipres complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HaciendaNotificaADipres">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="expediente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estadoTramitacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="decreto" type="{http://caroli.reddipres.cl/pkg_hacienda.Notificacion_Hacienda_a_Dipres}decreto"/>
 *         &lt;element name="antecedente" type="{http://caroli.reddipres.cl/pkg_hacienda.Notificacion_Hacienda_a_Dipres}antecedente"/>
 *         &lt;element name="motivo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HaciendaNotificaADipres", propOrder = {
    "expediente",
    "estadoTramitacion",
    "decreto",
    "antecedente",
    "motivo"
})
public class HaciendaNotificaADipres {

    @XmlElement(required = true, nillable = true)
    protected String expediente;
    @XmlElement(required = true, nillable = true)
    protected String estadoTramitacion;
    @XmlElement(required = true, nillable = true)
    protected Decreto decreto;
    @XmlElement(required = true, nillable = true)
    protected Antecedente antecedente;
    @XmlElement(required = true, nillable = true)
    protected String motivo;

    /**
     * Gets the value of the expediente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpediente() {
        return expediente;
    }

    /**
     * Sets the value of the expediente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpediente(String value) {
        this.expediente = value;
    }

    /**
     * Gets the value of the estadoTramitacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoTramitacion() {
        return estadoTramitacion;
    }

    /**
     * Sets the value of the estadoTramitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoTramitacion(String value) {
        this.estadoTramitacion = value;
    }

    /**
     * Gets the value of the decreto property.
     * 
     * @return
     *     possible object is
     *     {@link Decreto }
     *     
     */
    public Decreto getDecreto() {
        return decreto;
    }

    /**
     * Sets the value of the decreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Decreto }
     *     
     */
    public void setDecreto(Decreto value) {
        this.decreto = value;
    }

    /**
     * Gets the value of the antecedente property.
     * 
     * @return
     *     possible object is
     *     {@link Antecedente }
     *     
     */
    public Antecedente getAntecedente() {
        return antecedente;
    }

    /**
     * Sets the value of the antecedente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Antecedente }
     *     
     */
    public void setAntecedente(Antecedente value) {
        this.antecedente = value;
    }

    /**
     * Gets the value of the motivo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * Sets the value of the motivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivo(String value) {
        this.motivo = value;
    }

}
