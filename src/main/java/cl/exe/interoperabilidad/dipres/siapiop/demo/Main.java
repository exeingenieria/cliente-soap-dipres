package cl.exe.interoperabilidad.dipres.siapiop.demo;

import cl.exe.interoperabilidad.dipres.siapiop.Antecedente;
import cl.exe.interoperabilidad.dipres.siapiop.Decreto;
import cl.exe.interoperabilidad.dipres.siapiop.HaciendaNotificaADipres;
import cl.exe.interoperabilidad.dipres.siapiop.Notificador;
import cl.exe.interoperabilidad.dipres.siapiop.ResponseSiapiop;

public class Main {

	public static void main(String[] args) throws Exception{
		try {
			Notificador notificador = new Notificador();
			HaciendaNotificaADipres req = new HaciendaNotificaADipres();
			
			req.setExpediente("E504/2018");
			req.setEstadoTramitacion("DEV");
			req.setDecreto(new Decreto());
			req.setAntecedente(new Antecedente());
			req.setMotivo("Motivo devolucion");
			
			ResponseSiapiop response = notificador.notificar(req);
			
			System.out.println("Codigo respuesta: " + response.getCodigo());
			System.out.println("Mensaje respuesta: " + response.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
