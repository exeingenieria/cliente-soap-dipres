package cl.exe.interoperabilidad.dipres.exception;

public class ConexionException extends Exception {

	private static final long serialVersionUID = 7563497677570896387L;

	public ConexionException(String msg) {
		super(msg);
	}

}
