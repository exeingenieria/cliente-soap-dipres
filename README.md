# Cliente SOAP DIPRES

Cliente SOAP para consumir WS de notificación de DIPRES.

## Requerimientos
 - Java 1.8
 - Maven

## Configuraciones
El archivo soap-dipres.properties incluye los datos de configuración del WSDL de DIPRES. 
Es necesario modificarlo para configurar el ambiente de producción.